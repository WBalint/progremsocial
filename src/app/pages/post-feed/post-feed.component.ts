import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreatePostComponent } from 'src/app/tools/create-post/create-post.component';
import { FirebaseTSFirestore, Limit, OrderBy, Where } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';

@Component({
  selector: 'app-post-feed',
  templateUrl: './post-feed.component.html',
  styleUrls: ['./post-feed.component.css']
})
export class PostFeedComponent implements OnInit {
  firestore = new FirebaseTSFirestore();
  posts: PostData [] = [];
  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
    this.getPosts();
  }

  onCreatePostClick()
  {
    this.dialog.open(CreatePostComponent);
  }

  getPosts()
  {
    this.firestore.getCollection(
      {
        path: ["Posts"],
        where: [
          //Since we want to get all post, remove the filter(where) to check the id
          //new Where("creatorId", "==", "cCgDF2v5e7fcAUxXzmX35rIJcj23"),     //== stands for get all posts from a specific creator
          new OrderBy("timestamp", "desc"),
          //Number of posts displayed
          new Limit(10)
        ],
        onComplete: result => {
          result.docs.forEach(
            doc => {
              let post = <PostData>doc.data();
              post.postId = doc.id;
              this.posts.push(post);
            }
          )
        }, 
        onFail: err => {

        }
      }
    );
  }
}


export interface PostData
{
  comment: string;
  creatorId: string;
  //imageUrl is an optional field so we use question mark
  imageUrl?: string;
  postId: string;
}