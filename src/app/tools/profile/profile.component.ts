import { Component, Input, OnInit } from '@angular/core';
import { FirebaseTSFirestore } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';
import { FirebaseTSStorage } from 'firebasets/firebasetsStorage/firebaseTSStorage';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Input() show: boolean | undefined;

  firestore: FirebaseTSFirestore;
  auth: FirebaseTSAuth;
  storage = new FirebaseTSStorage();
  selectedImageFile: File;
  userHasProfile = false;

  constructor() {
    this.firestore = new FirebaseTSFirestore();
    this.auth = new FirebaseTSAuth();
  }

  ngOnInit(): void {

  }

  onContinueClick(
    nameInput: HTMLInputElement,
    descriptionInput: HTMLTextAreaElement
  ) {
    let name = nameInput.value;
    let desc = descriptionInput.value;

    //If comment is empty return the function   
    if (name.length <= 0 && desc.length <= 0) {
      this.show = false;
      return;
    }

    this.getUserProfile(nameInput, descriptionInput);
  }

  editProfileWidthImage(
    nameInput: HTMLInputElement,
    descriptionInput: HTMLTextAreaElement
  ) {
    let name = nameInput.value;
    let description = descriptionInput.value;

    //this.firestore.getDocument(`ProfileImages/${this.auth.getAuth().currentUser.uid}`)
    this.storage.upload(
      {
        uploadName: "upload Profile Image",
        path: ["ProfileImages", this.auth.getAuth().currentUser.uid],
        data: {
          data: this.selectedImageFile
        },
        onComplete: (downloadUrl) => {
          this.firestore.update(
            {
              path: ["Users", this.auth.getAuth().currentUser.uid],
              data: {
                publicName: name,
                description: description,
                imageUrl: downloadUrl,
                userId: this.auth.getAuth().currentUser.uid
              },
              onComplete: (docId) => {
                AppComponent.getRefreshed();
                //window.location.reload();
              }
            }
          );
          alert("Profile Edited");
          nameInput.value = "";
          descriptionInput.value = "";
          this.show = false;
        },
        onFail: (err) => {
          alert("Failed to edit profile");
        }
      }
    );
  }

  editProfile(
    nameInput: HTMLInputElement,
    descriptionInput: HTMLTextAreaElement
  ) {
    let name = nameInput.value;
    let description = descriptionInput.value;
    this.firestore.update(
      {
        path: ["Users", this.auth.getAuth().currentUser.uid],
        data: {
          publicName: name,
          description: description,
          userId: this.auth.getAuth().currentUser.uid
        },
        onComplete: (docId) => {
          AppComponent.getRefreshed();
          //window.location.reload();
          alert("Profile Edited");
        }
      }
    );
    //alert("Profile Edited");
    nameInput.value = "";
    descriptionInput.value = "";
    this.show = false;

  }

  createProfileWidthImage(
    nameInput: HTMLInputElement,
    descriptionInput: HTMLTextAreaElement
  ) {
    let name = nameInput.value;
    let description = descriptionInput.value;

    //this.firestore.getDocument(`ProfileImages/${this.auth.getAuth().currentUser.uid}`)
    this.storage.upload(
      {
        uploadName: "upload Profile Image",
        path: ["ProfileImages", this.auth.getAuth().currentUser.uid],
        data: {
          data: this.selectedImageFile
        },
        onComplete: (downloadUrl) => {
          this.firestore.create(
            {
              path: ["Users", this.auth.getAuth().currentUser.uid],
              data: {
                publicName: name,
                description: description,
                imageUrl: downloadUrl,
                userId: this.auth.getAuth().currentUser.uid
              },
              onComplete: (docId) => {
                AppComponent.getRefreshed();
                //window.location.reload();
              }
            }
          );
          alert("Profile created");
          nameInput.value = "";
          descriptionInput.value = "";
          this.show = false;
        },
        onFail: (err) => {
          alert("Failed to create profile");
        }
      }
    );
  }

  createProfile(
    nameInput: HTMLInputElement,
    descriptionInput: HTMLTextAreaElement
  ) {
    let name = nameInput.value;
    let description = descriptionInput.value;
    this.firestore.create(
      {
        path: ["Users", this.auth.getAuth().currentUser.uid],
        data: {
          publicName: name,
          description: description,
          userId: this.auth.getAuth().currentUser.uid
        },
        onComplete: (docId) => {
          AppComponent.getRefreshed();
          //window.location.reload();
        }
      }
    );
    alert("Profile Created");
    nameInput.value = "";
    descriptionInput.value = "";
    this.show = false;

  }

  onPhotoSelected(photoSelector: HTMLInputElement) {
    this.selectedImageFile = photoSelector.files[0];

    //if no image selected prevent from crushing
    if (!this.selectedImageFile) return;
    let fileReader = new FileReader();
    fileReader.readAsDataURL(this.selectedImageFile);
    fileReader.addEventListener(
      "loadend",
      evt => {
        let readableString = fileReader.result.toString();
        let postPreviewImage = <HTMLImageElement>document.getElementById("post-preview-image");
        postPreviewImage.src = readableString;
      }
    );
  }

  getUserProfile(
    nameInput: HTMLInputElement,
    descriptionInput: HTMLTextAreaElement
  ) {
    this.firestore.listenToDocument(
      {
        name: "Getting Document",
        path: ["Users", this.auth.getAuth().currentUser.uid],
        onUpdate: (result) => {
          this.userHasProfile = result.exists;

          if (this.userHasProfile) {
            //alert("Van profil " + nameInput);
            
            if (this.selectedImageFile) {
              this.editProfileWidthImage(nameInput, descriptionInput);
            } else {
              this.editProfile(nameInput, descriptionInput);
            }
          } else {
            //alert("Nincs profil");
      
            if (this.selectedImageFile) {
              this.createProfileWidthImage(nameInput, descriptionInput);
            } else {
              this.createProfile(nameInput, descriptionInput);
            }
          }
        }
      }
    );
  }
}