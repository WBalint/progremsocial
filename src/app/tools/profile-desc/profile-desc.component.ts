import { Component, Inject, Input, OnInit } from '@angular/core';
import { PostData } from 'src/app/pages/post-feed/post-feed.component';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FirebaseTSFirestore } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';

@Component({
  selector: 'app-profile-desc',
  templateUrl: './profile-desc.component.html',
  styleUrls: ['./profile-desc.component.css']
})
export class ProfileDescComponent implements OnInit {
  @Input() postData: PostData;
  userName: string;
  userPic: string;
  userDesc: string;
  firestore = new FirebaseTSFirestore();

  constructor(@Inject(MAT_DIALOG_DATA) private creatorId: string) { }

  ngOnInit(): void {
    this.getProfileInfo();
  }

  getProfileInfo()
  {
    this.firestore.getDocument(
      {
        path: ["Users", this.creatorId],
        onComplete: result => {
          let userDocument = result.data();
          this.userPic = userDocument['imageUrl'];
          this.userName = userDocument['publicName'];
          this.userDesc = userDocument['description'];
        },
        onFail: err => { 
          alert("Profil nem található") 
        }
      }
    );
  }

}

