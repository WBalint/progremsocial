import { Component, OnInit } from '@angular/core';
import { FirebaseTSFirestore } from "firebasets/firebasetsFirestore/firebaseTSFirestore";
import { FirebaseTSAuth } from "firebasets/firebasetsAuth/firebaseTSAuth";

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  firestore = new FirebaseTSFirestore();
  auth = new FirebaseTSAuth();
  userName: string;
  userDesc: string;
  userProfileImg: string;
  editProfile = false;

  constructor() {
    this.auth.listenToSignInStateChanges(
      user => {
        this.auth.checkSignInState(
          {
            whenSignedIn: user => {
              this.getUserInfo();
            }
          }
        );
      }
    );
  }

  ngOnInit(): void { 
  }

  profileSettings() {
    this.editProfile = true;
  }

  getUserInfo() {
    this.firestore.getDocument({
      path: ["Users", this.auth.getAuth().currentUser.uid],
      onComplete: (result) => {
        let userDocument = result.data();
        this.userName = userDocument["publicName"];
        this.userDesc = userDocument["description"];
        this.userProfileImg = userDocument["imageUrl"];
      },
    });
  }

}
