import { Component } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { AuthenticatorComponent } from './tools/authenticator/authenticator.component';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';
import { Router } from '@angular/router';
import { FirebaseTSFirestore } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProgRemSocial';
  auth = new FirebaseTSAuth();
  firestore = new FirebaseTSFirestore();
  userHasProfile = true;
  private static userDocument: UserDocument;
  isLogged = false;

  constructor(private loginSheet: MatBottomSheet,
    private router: Router
    ){
    this.auth.listenToSignInStateChanges(
      user => {
        this.auth.checkSignInState(
          {
            whenSignedIn: user => {
              this.getUserProfile();
            },
            whenSignedOut: user => {
              AppComponent.userDocument = null;
              this.router.navigate(["home"]);
            },
            whenSignedInAndEmailNotVerified: user => {
              
            },
            whenSignedInAndEmailVerified: user => {
              
            },
            whenChanged: user => {

            }  
          }
        );
      }
    );
  }
  getUserProfile()
  {
    this.firestore.listenToDocument(
      {
        name: "Getting Document",
        path: ["Users", this.auth.getAuth().currentUser.uid],
        onUpdate: (result) => {
          AppComponent.userDocument = <UserDocument>result.data();
          this.userHasProfile = result.exists;
          AppComponent.userDocument.userId = this.auth.getAuth().currentUser.uid;
          if(this.userHasProfile)
          {
            this.router.navigate(["postfeed"]);
          }
        }
      }
    );
  }
  public static getUserDocument(){
    return AppComponent.userDocument;
  }

  public static getRefreshed(){
    return window.location.reload();
  }

  getUserName(){
      return AppComponent.userDocument.publicName;
   }

onLogoutClick(){
  this.auth.signOut();
  AppComponent.getRefreshed();
}

  loggedIn(){
    return this.auth.isSignedIn();
  }

  onLoginClick(){
    this.loginSheet.open(AuthenticatorComponent);
  }
}


export interface UserDocument{
  publicName: string;
  description: string;
  userId: string;
}