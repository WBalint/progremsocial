// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD30SRfPkNULzy4IMatPEV-WqR0ATuRMbE",
    authDomain: "progremsocial.firebaseapp.com",
    projectId: "progremsocial",
    storageBucket: "progremsocial.appspot.com",
    messagingSenderId: "417481317294",
    appId: "1:417481317294:web:d032be8d4159c41cc4863f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
